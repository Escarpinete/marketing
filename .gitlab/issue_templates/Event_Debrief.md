### Event Debrief

Overall feedback about the event. Was the location okay? Was it hard to get to? Was it well organized?

Overview of who was there (Developers? C-levels? Non-technical people?)

Were there any competitors there? What was their presence like?

Who did you speak to that we need to follow up with? Did you meet any customer/hiring prospects?

What went well?

What didn't go so well?

What can we improve?

Would you attend/sponsor/speak at this event again?

### Talk

Event Name:
Event Dates:
Event Location:

Talk Title:
Talk Presenter(s):
Talk Slides (if available online somewhere):
Talk Abstract:
Talk Time (include timezone):
Attendee Estimate (how many people attended your session?):

How was the talk received by the audience?

What went well?

What didn't go so well?

Were there any technical difficulties?

What questions did people ask?
